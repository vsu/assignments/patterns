from abc import ABCMeta, abstractmethod


class CPU(metaclass=ABCMeta):
    @abstractmethod
    def __repr__(self):
        raise NotImplementedError()


class Motherboard(metaclass=ABCMeta):
    @abstractmethod
    def __repr__(self):
        raise NotImplementedError()


class SSD(metaclass=ABCMeta):
    @abstractmethod
    def __repr__(self):
        raise NotImplementedError()


class GPU(metaclass=ABCMeta):
    @abstractmethod
    def __repr__(self):
        raise NotImplementedError()


"""
Абстракная фабрика
"""
class PCFactory(metaclass=ABCMeta):
    @abstractmethod
    def make_cpu(self) -> CPU:
        raise NotImplementedError()

    @abstractmethod
    def make_motherboard(self) -> Motherboard:
        raise NotImplementedError()

    @abstractmethod
    def make_sdd(self) -> SSD:
        raise NotImplementedError()

    @abstractmethod
    def make_gpu(self) -> GPU:
        raise NotImplementedError()


"""
Конкретная фабрика 1
"""
class PowerfullCPU(CPU):
    def __repr__(self):
        print('Intel i7')


class PowerfullMotherboad(Motherboard):
    def __repr__(self):
        print('Overclocker ASUS')


class PowerfullGPU(GPU):
    def __repr__(self):
        print('GTX 1080')


class FastSSD(SSD):
    def __repr__(self):
        print('Fast SAMSUNG SSD')


class GamePCFactory(PCFatory):
    def make_cpu(self) -> CPU:
        return PowerfullCPU()

    def make_motherboard(self) -> Motherboard:
        return PowerfullMotherboad()

    def make_ssd(self) -> SSD:
        return FastSSD()

    def make_gpu(self) -> GPU:
        return PowerfullGPU()


"""
Конкретная фабрика 2
"""


class LowPriceCPU(CPU):
    def __repr__(self):
        print('Intel Celeron')


class LowPriceMotherboad(Motherboard):
    def __repr__(self):
        print('mini ATX Gigabyte')


class LowPriceGPU(GPU):
    def __repr__(self):
        print('Radeon RX2')


class LowPriceSSD(SSD):
    def __repr__(self):
        print('Slow Corsair SSD')


class OfficePCFactory(PCFatory):
    def make_cpu(self) -> CPU:
        return LowPriceCPU()

    def make_motherboard(self) -> Motherboard:
        return LowPriceMotherboad()

    def make_ssd(self) -> SSD:
        return LowPriceSSD()

    def make_gpu(self) -> GPU:
        return LowPriceGPU()
