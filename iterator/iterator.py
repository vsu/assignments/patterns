from abc import ABCMeta, abstractmethod


class Iterator(metaclass=ABCMeta):
    def __init__(self, collection, cursor=0):
        self._collection = collection
        self._cursor = cursor

    @abstractmethod
    def current_item(self):
        raise NotImplementedError()

    @abstractmethod
    def first(self):
        raise NotImplementedError()

    @abstractmethod
    def next(self):
        raise NotImplementedError()

    @abstractmethod
    def has_next(self):
        raise NotImplementedError()


class Aggregate(metaclass=ABCMeta):
    @abstractmethod
    def iterator(self):
        raise NotImplementedError()


class ListIterator(Iterator):
    def __init__(self, collection):
        super().__init__(collection, 0)

    def current(self):
        if self._cursor < len(self._collection):
            return self._collection[self._cursor]
        raise IndexError()

    def first(self):
        return self._collection[0]

    def next(self):
        if self.has_next():
            self._cursor += 1
            return self._collection[self._cursor]
        raise IndexError()

    def has_next(self):
        return len(self._collection) >= self._cursor + 1


class ListCollection(Aggregate):
    def __init__(self, collection):
        self._collection = collection

    def iterator(self):
        return ListIterator(self._collection)


if __name__ == '__main__':
    ex_list = ListCollection([1, 1, 2, 3, 5, 8, 12])
    iterator = ex_list.iterator()
    print(iterator.current())
    print(iterator.next())
    print(iterator.next())
    print(iterator.next())
    print(iterator.has_next())
