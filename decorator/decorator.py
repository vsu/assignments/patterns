import json
from abc import ABCMeta, abstractmethod


class Parser(metaclass=ABCMeta):
    @abstractmethod
    def parse_entity(self, data):
        raise NotImplementedError()


class JsonParser(Parser):
    def parse_entity(self, data):
        result = json.loads(data)
        return result


class SafeJsonParser(Parser):
    def __init__(self, parser):
        self._parser = parser

    def parse_entity(self, data):
        if data is None:
            return None
        return self._parser.parse_entity(data)


class VerySafeJsonParser(Parser):
    def __init__(self, parser):
        self._parser = parser

    def parse_entity(self, data):
        try:
            result = self._parser.parse_entity(data)
        except ValueError:
            print("Decode error")
            return None
        return result

class AddSomeKeys(Parser):
    def __init__(self, parser):
        self._parser = parser

    def parse_entity(self, data):
        result = self._parser.parse_entity(data)
        return result.setdefault('extra', 'some data')
