import hashlib
from abc import ABCMeta, abstractmethod


class PasswordSaver(metaclass=ABCMeta):
    @abstractmethod
    def get_hash_algo(self):
        raise NotImplementedError

    def save_password(self, password, filename):
        hash_algo = self.get_hash_algo()
        hash_password = hash_algo.hash(password)
        with open(filename, 'w') as f:
            f.write(hash_password)
        return True


class Hash(metaclass=ABCMeta):
    @abstractmethod
    def hash(self, raw):
        raise NotImplementedError


class MD5Hash(Hash):
    def hash(self, raw):
        m = hashlib.md5()
        m.update(raw.encode('utf-8'))
        return m.hexdigest()


class SHA1Hash(Hash):
    def hash(self, raw):
        m = hashlib.sha1()
        m.update(raw.encode('utf-8'))
        return m.hexdigest()


class MD5PasswordSaver(PasswordSaver):
    def get_hash_algo(self):
        return MD5Hash()


class SHA1PasswordSaver(PasswordSaver):
    def get_hash_algo(self):
        return SHA1Hash()


if __name__ == '__main__':
    ex = MD5PasswordSaver()
    ex.save_password('124124', 'test.txt')
