import pytest
from singleton.singleton import Singleton

@pytest.fixture
def ex_class():
    class Example(metaclass=Singleton):
        pass

    return Example

def test_singleton(ex_class):
    ex1 = ex_class()
    ex2 = ex_class()
    assert ex1 == ex2
