from abc import ABCMeta, abstractmethod


class Observer(metaclass=ABCMeta):
    @abstractmethod
    def update(self, data):
        raise NotImplementedError()


class Observable(metaclass=ABCMeta):
    def __init__(self):
        self._observers = []

    @abstractmethod
    def subscribe(self, observer):
        raise NotImplementedError()

    @abstractmethod
    def send_data(self, data):
        raise NotImplementedError()


class RSSMailing(Observable):
    def subscribe(self, observer):
        self._observers.append(observer)

    def send_data(self, data):
        for observer in self._observers:
            observer.update(data)

    def add_post(self, post):
        self.send_data(post)


class Subscriber(Observer):
    def __init__(self, login):
        self.login = login

    def update(self, message):
        print('{} recieved "{}"'.format(self.login, message))


if __name__ == '__main__':
    rss = RSSMailing()
    rss.subscribe(Subscriber('Citizen'))
    rss.subscribe(Subscriber('Four'))
    rss.subscribe(Subscriber('Revolutionär'))
    rss.add_post('Hello World!')
