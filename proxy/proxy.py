import os
from abc import ABCMeta, abstractmethod

import requests


class Render(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self, url):
        self._url = url

    @abstractmethod
    def render_wireframe(self, model):
        raise NotImplementedError()

    @abstractmethod
    def render_with_lighting(self, model, pos):
        raise NotImplementedError()

    @abstractmethod
    def render_with_texture(self, model, texture):
        raise NotImplementedError()


class RemoteRender(Render):
    def render_wireframe(self, model):
        path = '/render_wireframe'
        files = {'file': open('model', 'rb')}
        response = requests.post('{}{}'.format(self._url, path), files=files)
        return response.content

    def render_with_lighting(self, model, pos):
        path = '{}&pos={}'.format('/render', pos)
        response = requests.post('{}{}'.format(self._url, path))
        return response.content

    def render_with_texture(self, model, texture):
        path = '/render'
        files = {
            'model': open('model', 'rb'),
            'texture': open('texture', 'rb')
        }
        response = requests.post('{}{}'.format(self._url, path), files=files)
        return response.content


class RenderProxy(Render):
    def render_wireframe(self, model):
        size = os.stat('model').st_size
        if size > 2048:
            if not self._render:
                self._render = Render(self._url)
            return self._render.render_wireframe(model)
        print('Local render')
        return 'Some content'

    def render_with_lighting(self, model, pos):
        if not self._render:
            self._render = RemoteRender(self._url)
        return self._render.render_with_lighting(model, pos)

    def render_with_texture(self, model, texture=None):
        if texture is None:
            return self.render_wireframe(model)
        if not self._render:
            self._render = RemoteRender(self._url)
        return self._render.render_with_texture(model, texture)
