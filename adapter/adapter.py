from abc import ABCMeta, abstractmethod

import requests


class Hipchat:
    def __init__(self,
                 base_url='https://vsu.hipchat.com/v2',
                 api_key='asdfehfuhJH#@UI$Hfjksdkf',
                 base_auth=('vsu', 'vsu_pass')):
        self._base_url = base_url
        self._api_key = api_key
        if base_auth:
            self._base_auth = base_auth
        elif api_key:
            self._api_key = api_key

    def _get_request_kwargs(self):
        kwargs = {
            'headers': {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
        if hasattr(self, '_base_auth'):
            kwargs['auth'] = HTTPBasicAuth(*_base_auth)
        elif hasattr(self, '_api_key'):
            kwargs['headers']['Authorization'] = 'Bearer {}'.format(
                self._api_key)
        return kwargs

    def push_message(self, user_id, message):
        path = '/room/{}/message'
        kwargs = self._get_request_kwargs()
        body = {'message': message, 'message_format': 'text'}
        response = requests.post('{}{}'.format(self._base_url, path), **kwargs)
        return response

    def push_notification(self, room_id, message):
        path = '/room/{}/notification'
        kwargs = self._get_request_kwargs()
        body = {'message': message, 'message_format': 'text', 'color': 'gray'}
        response = requests.post('{}{}'.format(self._base_url, path), **kwargs)
        return response


class Stride:
    def __init__(self,
                 url='https://api.stride.com',
                 auth_key='asfjjewrhw324',
                 credentials=None):
        self._url = url
        self._auth_key = auth_key
        self._credentials = credentials

    def _get_kwargs(self):
        kwargs = {
            'headers': {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
        if hasattr(self, '_base_auth'):
            kwargs['auth'] = HTTPBasicAuth(*self._credentials)
        elif hasattr(self, '_api_key'):
            kwargs['headers']['Authorization'] = 'Bearer {}'.format(
                self._auth_key)
        return kwargs

    def message(self, id, message):
        path = '/site/3543/conversations/user/{}/message'
        kwargs = self._get_kwargs()
        body = {'message': message}
        response = requests.post('{}{}'.format(self._url, path), **kwargs)
        return response

    def notification(self, id, message):
        path = '/site/3543/conversations/{}/message'
        kwargs = self._get_kwargs()
        body = {'message': message, 'message_format': 'text'}
        response = requests.post('{}{}'.format(self._base_url, path), **kwargs)
        return response


class Messenger(metaclass=ABCMeta):
    @abstractmethod
    def send_message(self, user, message):
        raise NotImplementedError()

    @abstractmethod
    def send_notification(self, room, message):
        raise NotImplementedError()


class StrideAdapter(Messenger):
    def __init__(self, api_url, base_auth, api_key):
        self._stride = Stride(api_url, api_key, base_auth)

    def send_message(self, user, message):
        return self._stride.message(user, message)

    def send_notification(self, room, message):
        return self._stride.notification(room, message)


class HipchatAdapter(Messenger):
    def __init__(self, api_url, base_auth, api_key):
        self._hipchat = Hipchat(api_url, base_auth, api_key)

    def send_message(self, user, message):
        self._hipchat.push_message(user, message)

    def send_notification(self, room, message):
        self._hipchat.push_notification(room, message)
