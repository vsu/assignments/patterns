from abc import ABCMeta, abstractmethod
from collections import namedtuple

import requests

JiraProject = namedtuple('JiraProject', ['title', 'project_key'])
Project = namedtuple('Project', ['jira_project', 'repo', 'room'])
Response = namedtuple('Response', ['status_code', 'data'])


class Hipchat:
    def create_room(self, name):
        # create room
        return Response(201, None)


class Jira:
    def create_project(self, name):
        response = requests.post('api.jira.vsu.com/projects/{}'.format(name))
        return JiraProject(name, response.json()['project_key'])


class Bitbucket:
    def create_repository(self, name):
        # create repo
        return Response(201, None)


class Project:
    def __init__(self):
        self._hipchat = Hipchat()
        self._jira = Jira()
        self._bitbucket = Bitbucket()

    def create(self, name):
        jira_project = self._jira.create_project('very cool')
        hipchat_room = self._hipchat.create_room(jira_project.project_key)
        bitbucket_repo = self._bitbucket.create_repository(jira_project.name)
        return Response(201, Project(jira_project, bitbucket_repo,
                                     hipchat_room))
